# Docker Compose File Details

These topics describe version 3 of the Compose file format. This is the newest
version when we composing this.

## Compose and Docker compatibility matrix

There are several versions of the Compose file format – 1, 2, 2.x, and 3.x. The
table below is a quick look. For full details on what each version includes and
how to upgrade, see **[About versions and upgrading](https://docs.docker.com/compose/compose-file/compose-versioning/)**.

**[About compose matrix ](https://docs.docker.com/compose/compose-file/compose-file-v3/#compose-and-docker-compatibility-matrix)**.


## Compose file structure and examples

Here is a sample Compose file from the voting app sample used in the
[Docker for Beginners lab](https://github.com/docker/labs/tree/master/beginner/)
topic on [Deploying an app to a Swarm](https://github.com/docker/labs/blob/master/beginner/chapters/votingapp.md):

The topics on this reference page are organized alphabetically by top-level key
to reflect the structure of the Compose file itself. Top-level keys that define
a section in the configuration file such as `build`, `deploy`, `depends_on`,
`networks`, and so on, are listed with the options that support them as
sub-topics. This maps to the `<key>: <option>: <value>` indent structure of the
Compose file.

## Service configuration reference

The Compose file is a [YAML](https://yaml.org) file defining
[services](https://docs.docker.com/compose/compose-file/compose-file-v3/#service-configuration-reference),
[networks](https://docs.docker.com/compose/compose-file/compose-file-v3/#network-configuration-reference) and
[volumes](https://docs.docker.com/compose/compose-file/compose-file-v3/#volume-configuration-reference).
The default path for a Compose file is `./docker-compose.yml`.

> **Tip**: You can use either a `.yml` or `.yaml` extension for this file.
> They both work.

A service definition contains configuration that is applied to each
container started for that service, much like passing command-line parameters to
`docker run`. Likewise, network and volume definitions are analogous to
`docker network create` and `docker volume create`.

As with `docker run`, options specified in the Dockerfile, such as `CMD`,
`EXPOSE`, `VOLUME`, `ENV`, are respected by default - you don't need to
specify them again in `docker-compose.yml`.

You can use environment variables in configuration values with a Bash-like
`${VARIABLE}` syntax - see [variable substitution](https://docs.docker.com/compose/compose-file/compose-file-v3/#variable-substitution) for
full details.

This section contains a list of all configuration options supported by a service
definition in version 3.

### build

Configuration options that are applied at build time.

`build` can be specified either as a string containing a path to the build
context:

```yaml
version: "3.9"
services:
  webapp:
    build: ./dir
```

Or, as an object with the path specified under [context](#context) and
optionally [Dockerfile](#dockerfile) and [args](#args):

```yaml
version: "3.9"
services:
  webapp:
    build:
      context: ./dir
      dockerfile: Dockerfile-alternate
      args:
        buildno: 1
```

If you specify `image` as well as `build`, then Compose names the built image
with the `webapp` and optional `tag` specified in `image`:

```yaml
build: ./dir
image: webapp:tag
```

This results in an image named `webapp` and tagged `tag`, built from `./dir`.

> Note when using docker stack deploy
>
> The `build` option is ignored when
> [deploying a stack in swarm mode](https://docs.docker.com/engine/reference/commandline/stack_deploy/)
> The `docker stack` command does not build images before deploying.
{: .important }

#### context

Either a path to a directory containing a Dockerfile, or a url to a git repository.

When the value supplied is a relative path, it is interpreted as relative to the
location of the Compose file. This directory is also the build context that is
sent to the Docker daemon.

Compose builds and tags it with a generated name, and uses that image
thereafter.

```yaml
build:
  context: ./dir
```

#### dockerfile

Alternate Dockerfile.

Compose uses an alternate file to build with. A build path must also be
specified.

```yaml
build:
  context: .
  dockerfile: Dockerfile-alternate
```

#### args

Add build arguments, which are environment variables accessible only during the
build process.

First, specify the arguments in your Dockerfile:

```dockerfile
ARG buildno
ARG gitcommithash

RUN echo "Build number: $buildno"
RUN echo "Based on commit: $gitcommithash"
```

Then specify the arguments under the `build` key. You can pass a mapping
or a list:

```yaml
build:
  context: .
  args:
    buildno: 1
    gitcommithash: cdc3b19
```

```yaml
build:
  context: .
  args:
    - buildno=1
    - gitcommithash=cdc3b19
```

> Scope of build-args
>
> In your Dockerfile, if you specify `ARG` before the `FROM` instruction,
> `ARG` is not available in the build instructions under `FROM`.
> If you need an argument to be available in both places, also specify it under
> the `FROM` instruction. Refer to the [understand how ARGS and FROM interact](../../engine/reference/builder.md#understand-how-arg-and-from-interact)
> section in the documentation for usage details.

You can omit the value when specifying a build argument, in which case its value
at build time is the value in the environment where Compose is running.

```yaml
args:
  - buildno
  - gitcommithash
```

> Tip when using boolean values
>
> YAML boolean values (`"true"`, `"false"`, `"yes"`, `"no"`, `"on"`,
> `"off"`) must be enclosed in quotes, so that the parser interprets them as
> strings.

#### network

> Added in [version 3.4](https://docs.docker.com/compose/compose-file/compose-versioning/#version-34) file format

Set the network containers connect to for the `RUN` instructions during
build.

```yaml
build:
  context: .
  network: host
```

```yaml
build:
  context: .
  network: custom_network_1
```

Use `none` to disable networking during build:

```yaml
build:
  context: .
  network: none
```

#### shm_size

> Added in [version 3.5](https://docs.docker.com/compose/compose-file/compose-versioning/#version-35) file format

Set the size of the `/dev/shm` partition for this build's containers. Specify
as an integer value representing the number of bytes or as a string expressing
a [byte value](#specifying-byte-values).

```yaml
build:
  context: .
  shm_size: '2gb'
```

```yaml
build:
  context: .
  shm_size: 10000000
```

### command

Override the default command.

```yaml
command: bundle exec thin -p 3000
```

The command can also be a list, in a manner similar to
[dockerfile](https://docs.docker.com/engine/reference/builder/#cmd):

```yaml
command: ["bundle", "exec", "thin", "-p", "3000"]
```

### container_name

Specify a custom container name, rather than a generated default name.

```yaml
container_name: my-web-container
```

Because Docker container names must be unique, you cannot scale a service beyond
1 container if you have specified a custom name. Attempting to do so results in
an error.

> Note when using docker stack deploy
>
> The `container_name` option is ignored when
> [deploying a stack in swarm mode](../../engine/reference/commandline/stack_deploy.md)
{: .important }

### depends_on

Express dependency between services. Service dependencies cause the following
behaviors:

- `docker-compose up` starts services in dependency order. In the following
  example, `db` and `redis` are started before `web`.
- `docker-compose up SERVICE` automatically includes `SERVICE`'s
  dependencies. In the example below, `docker-compose up web` also
  creates and starts `db` and `redis`.
- `docker-compose stop` stops services in dependency order. In the following
  example, `web` is stopped before `db` and `redis`.

Simple example:

```yaml
version: "3.9"
services:
  web:
    build: .
    depends_on:
      - db
      - redis
  redis:
    image: redis
  db:
    image: postgres
```

> There are several things to be aware of when using `depends_on`:
>
> - `depends_on` does not wait for `db` and `redis` to be "ready" before
>   starting `web` - only until they have been started. If you need to wait
>   for a service to be ready, see [Controlling startup order](https://docs.docker.com/compose/startup-order/)
>   for more on this problem and strategies for solving it.
> - Version 3 no longer supports the `condition` form of `depends_on`.
> - The `depends_on` option is ignored when
>   deploying a stack in swarm mode
>   with a version 3 Compose file.

### deploy

> Added in version 3 file format.

Specify configuration related to the deployment and running of services. This
only takes effect when deploying to a swarm with
docker stack deploy, and is
ignored by `docker-compose up` and `docker-compose run`.

```yaml
version: "3.9"
services:
  redis:
    image: redis:alpine
    deploy:
      replicas: 6
      placement:
        max_replicas_per_node: 1
      update_config:
        parallelism: 2
        delay: 10s
      restart_policy:
        condition: on-failure
```

Several sub-options are available:

#### endpoint_mode

> Added in version 3.2 file format.

Specify a service discovery method for external clients connecting to a swarm.

* `endpoint_mode: vip` - Docker assigns the service a virtual IP (VIP)
that acts as the front end for clients to reach the service on a
network. Docker routes requests between the client and available worker
nodes for the service, without client knowledge of how many nodes
are participating in the service or their IP addresses or ports.
(This is the default.)

* `endpoint_mode: dnsrr` -  DNS round-robin (DNSRR) service discovery does
not use a single virtual IP. Docker sets up DNS entries for the service
such that a DNS query for the service name returns a list of IP addresses,
and the client connects directly to one of these. DNS round-robin is useful
in cases where you want to use your own load balancer, or for Hybrid
Windows and Linux applications.

```yaml
version: "3.9"

services:
  wordpress:
    image: wordpress
    ports:
      - "8080:80"
    networks:
      - overlay
    deploy:
      mode: replicated
      replicas: 2
      endpoint_mode: vip

  mysql:
    image: mysql
    volumes:
       - db-data:/var/lib/mysql/data
    networks:
       - overlay
    deploy:
      mode: replicated
      replicas: 2
      endpoint_mode: dnsrr

volumes:
  db-data:

networks:
  overlay:
```

The options for `endpoint_mode` also work as flags on the swarm mode CLI command
docker service create]. For a
quick list of all swarm related `docker` commands, see
Swarm mode CLI commands.

To learn more about service discovery and networking in swarm mode, see
Configure service discovery
in the swarm mode topics.


#### labels

Specify labels for the service. These labels are *only* set on the service,
and *not* on any containers for the service.

```yaml
version: "3.9"
services:
  web:
    image: web
    deploy:
      labels:
        com.example.description: "This label will appear on the web service"
```

To set labels on containers instead, use the `labels` key outside of `deploy`:

```yaml
version: "3.9"
services:
  web:
    image: web
    labels:
      com.example.description: "This label will appear on all containers for the web service"
```

#### mode

Either `global` (exactly one container per swarm node) or `replicated` (a
specified number of containers). The default is `replicated`. (To learn more,
see [Replicated and global services](https://docs.docker.com/engine/swarm/how-swarm-mode-works/services/#replicated-and-global-services)
in the [swarm](https://docs.docker.com/engine/swarm/) topics.)


```yaml
version: "3.9"
services:
  worker:
    image: dockersamples/examplevotingapp_worker
    deploy:
      mode: global
```

### image

Specify the image to start the container from. Can either be a repository/tag or
a partial image ID.

```yaml
image: redis
```
```yaml
image: ubuntu:18.04
```
```yaml
image: tutum/influxdb
```
```yaml
image: example-registry.com:4000/postgresql
```
```yaml
image: a4bc65fd
```

If the image does not exist, Compose attempts to pull it, unless you have also
specified [build](#build), in which case it builds it using the specified
options and tags it with the specified tag.

### restart

`no` is the default [restart policy](../../config/containers/start-containers-automatically.md#use-a-restart-policy), and it does not restart a container under
any circumstance. When `always` is specified, the container always restarts. The
`on-failure` policy restarts a container if the exit code indicates an
on-failure error. `unless-stopped` always restarts a container, except when the
container is stopped (manually or otherwise).

    restart: "no"
    restart: always
    restart: on-failure
    restart: unless-stopped

> Note when using docker stack deploy
>
> The `restart` option is ignored when
> [deploying a stack in swarm mode](../../engine/reference/commandline/stack_deploy.md).
{: .important }

### volumes

Mount host paths or named volumes, specified as sub-options to a service.

You can mount a host path as part of a definition for a single service, and
there is no need to define it in the top level `volumes` key.

But, if you want to reuse a volume across multiple services, then define a named
volume in the [top-level `volumes` key](#volume-configuration-reference). Use
named volumes with [services, swarms, and stack
files](#volumes-for-services-swarms-and-stack-files).

> Changed in [version 3](compose-versioning.md#version-3) file format.
>
> The top-level [volumes](#volume-configuration-reference) key defines
> a named volume and references it from each service's `volumes` list. This
> replaces `volumes_from` in earlier versions of the Compose file format.

This example shows a named volume (`mydata`) being used by the `web` service,
and a bind mount defined for a single service (first path under `db` service
`volumes`). The `db` service also uses a named volume called `dbdata` (second
path under `db` service `volumes`), but defines it using the old string format
for mounting a named volume. Named volumes must be listed under the top-level
`volumes` key, as shown.

```yaml
version: "3.9"
services:
  web:
    image: nginx:alpine
    volumes:
      - type: volume
        source: mydata
        target: /data
        volume:
          nocopy: true
      - type: bind
        source: ./static
        target: /opt/app/static

  db:
    image: postgres:latest
    volumes:
      - "/var/run/postgres/postgres.sock:/var/run/postgres/postgres.sock"
      - "dbdata:/var/lib/postgresql/data"

volumes:
  mydata:
  dbdata:
```

> **Note**
> 
> For general information on volumes, refer to the use volumes
> and volume plugins sections in the documentation.

## Compose documentation

- [User guide](https://docs.docker.com/compose/)
- [Installing Compose](https://docs.docker.com/compose/install/)
- [Sample apps with Compose](https://docs.docker.com/compose/samples-for-compose/)